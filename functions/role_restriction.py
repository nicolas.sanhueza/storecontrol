from flask import session, redirect
from functools import wraps

def role_required(*roles):
    def wrapper(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            # verificar si el usuario tiene permiso para acceder a la ruta
            if 'tipo_empleado' in session and session['tipo_empleado'] in roles:
                return func(*args, **kwargs)
            else:
                return redirect('/buscar')
        return decorated_view
    return wrapper