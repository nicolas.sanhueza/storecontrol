import database as db

def delete_record(table, id):
    cursor = db.database.cursor()
    sql = f'DELETE FROM {table} WHERE id = %s'
    cursor.execute(sql, (id,))
    db.database.commit()