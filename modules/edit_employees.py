from flask import Flask, render_template, request, redirect
import database as db

app = Flask(__name__)

@app.route('/edit_employees/<int:id>', methods=['GET', 'POST'])
def edit_employees(id):
    if request.method == 'GET':
        cursor = db.database.cursor()
        sql = "SELECT * FROM empleado WHERE id = %s"
        data = (id,)
        cursor.execute(sql, data)
        cursor.fetchone()
        return redirect ('/lista_empleados')

    if request.method == 'POST':
        nombre = request.form['nombre']
        ap_paterno = request.form['ap_paterno']
        ap_materno = request.form['ap_materno']
        email = request.form['email']
        contraseña = request.form['contraseña']
        tipo_empleado = request.form['tipo_empleado']
        cursor = db.database.cursor()
        sql = "UPDATE empleado SET nombre = %s, ap_paterno = %s, ap_materno = %s, email = %s, contraseña = %s, tipo_empleado = %s WHERE id = %s"
        data = (nombre, ap_paterno, ap_materno, email, contraseña, tipo_empleado, id)
        cursor.execute(sql, data)
        db.database.commit()
        return redirect('/lista_empleados')
