from flask import Flask, render_template, request
import database as db
from functions.role_restriction import role_required

app = Flask(__name__)

@app.route('/lista_empleados', methods=['GET', 'POST'])
@role_required('Administrador')
def lista_empleados():
    if request.method == 'GET':
        cursor = db.database.cursor()
        sql = "SELECT * FROM empleado"
        cursor.execute(sql)
        empleados = cursor.fetchall()
        return render_template('auth/empleados.html', empleados=empleados)