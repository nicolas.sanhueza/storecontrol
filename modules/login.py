from flask import Flask, render_template, request, session, redirect
import database as db

app = Flask(__name__)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template ('auth/login.html')

    if request.method == 'POST':
        email = request.form['email']
        contraseña = request.form['contraseña']
        cursor = db.database.cursor()
        sql = 'SELECT * FROM empleado WHERE email=%s AND contraseña=%s'
        data = (email, contraseña)
        cursor.execute(sql, data)
        result = cursor.fetchone()
        if result:
            # si las credenciales son correctas, guardar el tipo de empleado en la sesión
            session['tipo_empleado'] = result[6]
            return redirect('/buscar')
        else:
            incorrect_password = True
            return render_template ('auth/login.html', incorrect_password=incorrect_password)
