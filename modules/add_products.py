from flask import Flask, render_template, request, redirect
import database as db
from functions.role_restriction import role_required

app = Flask(__name__)

@app.route('/add_producto', methods=['GET', 'POST'])
@role_required('Administrador', 'Bodeguero')
def add_producto():
    if request.method == 'GET':
        return render_template ('auth/add_producto.html')
    
    if request.method == 'POST':
        id = request.form['id']
        capacidad = request.form['capacidad']
        marca = request.form['marca']
        tipo_producto = request.form['tipo_producto']
        stock = request.form['stock']
        backroom = request.form['backroom']

        if id and capacidad and marca and tipo_producto and stock and backroom:
            cursor = db.database.cursor()
            sql = "INSERT INTO producto (id, capacidad, marca, tipo_producto, stock, backroom) VALUES (%s, %s, %s, %s, %s, %s)"
            data = (id, capacidad, marca, tipo_producto, stock, backroom)
            cursor.execute(sql, data)
            db.database.commit()
            return redirect('/add_producto')