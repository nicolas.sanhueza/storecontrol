from flask import Flask, redirect
from functions.delete import delete_record

app = Flask(__name__)

@app.route('/delete/<int:id>', methods=['POST'])
def eliminar_empleado(id):
    delete_record('empleado', id)
    return redirect('/lista_empleados')
