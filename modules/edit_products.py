from flask import Flask, render_template, request
import database as db

app = Flask(__name__)

@app.route('/edit_produts/<int:id>', methods=['GET', 'POST'])
def edit_products(id):
    if request.method == 'GET':
        cursor = db.database.cursor()
        sql = "SELECT * FROM producto WHERE id = %s, "
        data = (id,)
        cursor.execute(sql, data)
        producto = cursor.fetchone()
        return render_template('auth/product_found.html', producto=producto)

    if request.method == 'POST':
        capacidad = request.form['capacidad']
        marca = request.form['marca']
        tipo_producto = request.form['tipo_producto']
        stock = request.form['stock']
        backroom = request.form['backroom']
        cursor = db.database.cursor()
        sql = "UPDATE producto SET capacidad = %s, marca = %s, tipo_producto = %s, stock = %s, backroom = %s WHERE id = %s"
        data = (capacidad, marca, tipo_producto, stock, backroom, id)
        cursor.execute(sql, data)
        db.database.commit()
        cursor = db.database.cursor()
        sql = "SELECT * FROM producto WHERE id = %s"
        data = (id,)
        cursor.execute(sql, data)
        producto = cursor.fetchone()
        idproducto_edited = True
        return render_template('auth/buscar.html', idproducto_edited=idproducto_edited)