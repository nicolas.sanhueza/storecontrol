from flask import Flask, render_template, request
import database as db

app = Flask(__name__)

@app.route('/buscar', methods=['GET', 'POST'])
def buscar_producto():
    if request.method == 'POST':
        id = request.form['id']
        cursor = db.database.cursor()
        sql = "SELECT * FROM producto WHERE id = %s"
        data = (id,)
        cursor.execute(sql, data)
        resultado = cursor.fetchone()
        
        if resultado is not None:
            producto = {"id": resultado[0], "capacidad": resultado[1], "marca": resultado[2], "tipo_producto": resultado[3], "stock": resultado[4], "backroom": resultado[5]}
            return render_template('auth/product_found.html', producto=producto)
        else:
            idproducto_unknown = True
            return render_template ('auth/buscar.html', idproducto_unknown=idproducto_unknown)
    return render_template('auth/buscar.html')