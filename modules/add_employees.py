from flask import Flask, render_template, request, redirect
import database as db
from functions.role_restriction import role_required

app = Flask(__name__)

@app.route('/add_empleado', methods=['GET', 'POST'])
@role_required('Administrador')
def add_empleado():
    if request.method == 'GET':
        return render_template ('auth/add_empleado.html')
    
    if request.method == 'POST':
        nombre = request.form['nombre']
        ap_paterno = request.form['ap_paterno']
        ap_materno = request.form['ap_materno']
        email = request.form['email']
        contraseña = request.form['contraseña']
        tipo_empleado = request.form['tipo_empleado']

        if nombre and ap_paterno and ap_materno and email and contraseña and tipo_empleado:
            cursor = db.database.cursor()
            sql = "INSERT INTO empleado (nombre, ap_paterno, ap_materno, email, contraseña, tipo_empleado) VALUES (%s, %s, %s, %s, %s, %s)"
            data = (nombre, ap_paterno, ap_materno, email, contraseña, tipo_empleado)
            cursor.execute(sql, data)
            db.database.commit()
            return redirect('/add_empleado')