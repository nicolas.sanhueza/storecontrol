from flask import Flask, redirect
from functions.delete import delete_record

app = Flask(__name__)

@app.route('/delete/<int:id>')
def eliminar_producto(id):
    delete_record('producto', id)
    return redirect('/buscar')
