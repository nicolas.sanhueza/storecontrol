from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from modules.login import login
from modules.delete_employees import eliminar_empleado
from modules.delete_products import eliminar_producto
from modules.add_employees import add_empleado
from modules.add_products import add_producto
from modules.search_product import buscar_producto
from modules.edit_products import edit_products
from modules.list_employees import lista_empleados
from modules.edit_employees import edit_employees

app = Flask(__name__)
app.secret_key = 'secret_key'
Bootstrap(app)

#--------REDIRECCIONA A LOGIN---------#
@app.route('/')
def index():
    return render_template('auth/login.html')

#---------------------LOGIN---------------------#
app.route('/login', methods=['GET', 'POST'])(login)

#---------------------DELETE EMPLOYEES---------------------#
app.route('/delete/<int:id>', methods=['POST'])(eliminar_empleado)

#---------------------DELETE PRODUCTS---------------------#
app.route('/delete/<int:id>')(eliminar_producto)

#---------------------ADD EMPLOYEES---------------------#
app.route('/add_empleado', methods=['GET', 'POST'])(add_empleado)

#---------------------AGREGAR PRODUCTO---------------------#
app.route('/add_producto', methods=['GET', 'POST'])(add_producto)

#---------------------SEARCH PRODUCTS---------------------#
app.route('/buscar', methods=['GET', 'POST'])(buscar_producto)

#---------------------EDITAR PRODUCTO---------------------#
app.route('/edit_produts/<int:id>', methods=['GET', 'POST'])(edit_products)

#---------------------EDITAR EMPLEADOS---------------------#
app.route('/edit_employees/<int:id>', methods=['GET', 'POST'])(edit_employees)

#--------LISTA DE EMPLEADOS--------#
app.route('/lista_empleados', methods=['GET', 'POST'])(lista_empleados)

if __name__ == '__main__':
    app.run(debug=True)
    